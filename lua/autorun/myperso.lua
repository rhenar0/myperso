if (SERVER) then   

    -- Net
    util.AddNetworkString("MyPerso")

    -- Config
    AddCSLuaFile("myperso/config.lua")

    -- Langues
    AddCSLuaFile("myperso/langues/french.lua")

    -- Func
    AddCSLuaFile("myperso/func/main/gen_menu_button_image.lua")
    AddCSLuaFile("myperso/func/main/gen_menu_button.lua")
    AddCSLuaFile("myperso/func/main/gen_menu_image.lua")
    AddCSLuaFile("myperso/func/main/gen_menu_window.lua")
    AddCSLuaFile("myperso/func/main/gen_menu_label.lua")
    AddCSLuaFile("myperso/func/main/gen_menu_window_image.lua")

    -- Menu
    AddCSLuaFile("myperso/menu/menu_myperso/ui_main.lua")

    concommand.Add( "uiopen", function( ply, cmd, args )
	    net.Start("MyPerso")
	    net.Send(ply)
    end)

elseif (CLIENT) then

    local ply = LocalPlayer()

    -- Config
    include("myperso/config.lua")

    -- Langues
    include("myperso/langues/french.lua")

    -- Func
    include("myperso/func/main/gen_menu_button_image.lua")
    include("myperso/func/main/gen_menu_button.lua")
    include("myperso/func/main/gen_menu_image.lua")
    include("myperso/func/main/gen_menu_window.lua")
    include("myperso/func/main/gen_menu_label.lua")
    include("myperso/func/main/gen_menu_window_image.lua")

    -- Menu
    include("myperso/menu/menu_myperso/ui_main.lua")

    net.Receive("MyPerso", function()
        myperso_main_frame()

        myperso_main_panel()
    end)

end