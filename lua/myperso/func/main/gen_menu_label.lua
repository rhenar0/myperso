function gen_menu_label(ply, text, posw, posh, font)

    local gen_menu_label = vgui.Create( "DLabel", myperso_main_frame )
    gen_menu_label:SetPos( posw, posh )
    gen_menu_label:SizeToContents()
    gen_menu_label:SetFont( font )
    gen_menu_label:SetText( text )

end