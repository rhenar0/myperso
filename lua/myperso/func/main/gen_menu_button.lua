function gen_menu_button(ply, sizew, sizeh, posw, posh, corner, color, funca)

    gen_menu_button = vgui.Create("DButton", myperso_main_frame)
    gen_menu_button:SetSize(sizew, sizeh)
    gen_menu_button:SetPos(posw, posh)
    gen_menu_button.OnMousePressed = funca
    gen_menu_button.Paint = function( self, w, h )
	    draw.RoundedBox( corner, 0, 0, w, h, color )
    end

end