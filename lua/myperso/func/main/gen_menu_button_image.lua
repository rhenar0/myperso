function gen_menu_button_image(ply, image, sizew, sizeh, posw, posh, corner, funca, color)

    gen_menu_button_image = vgui.Create("DImageButton", myperso_main_frame)
    gen_menu_button_image:SetSize(sizew, sizeh)
    gen_menu_button_image:SetPos(posw, posh)
    gen_menu_button_image:SetImage(image)
    gen_menu_button_image.OnMousePressed = funca

end