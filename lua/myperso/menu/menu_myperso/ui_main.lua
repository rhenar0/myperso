local ply = LocalPlayer()

function myperso_main_frame()

    myperso_main_frame = vgui.Create("DFrame")
    myperso_main_frame:SetSize(ScrW(), ScrH())
    myperso_main_frame:Center()
    myperso_main_frame:MakePopup()
    myperso_main_frame:SetVisible(true)
    myperso_main_frame:ShowCloseButton(true)
    myperso_main_frame:SetDraggable(false)
    myperso_main_frame.Paint = function(self, w,h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0,0,0,0))
    end

end

-- Background
function myperso_background() local image = "" gen_menu_window_image(ply, image) end

-- Bar Infos
function myperso_infos_server_frame() local sizeh = ScrH()*0.1 local sizew = ScrW()*0.75 local posh = ScrH()*0.08 local posw = ScrW()*0.1 local corner = 0 local color = Color(0, 128, 255) gen_menu_window(ply, sizew, sizeh, posw, posh, corner, color) end
function myperso_infos_server_label_name() local text = ""..MyPerso.Langue.playon.." "..GetHostName() local posw = ScrW()*0.12 local posh = ScrH()*0.09 local font = "DermaDefault" gen_menu_label(ply, text, posw, posh, font) end
function myperso_infos_server_label_slots() local text = ""..MyPerso.Langue.players.." : "..player.GetCount().."/"..game.MaxPlayers() local posw = ScrW()*0.8 local posh = ScrH()*0.09 local font = "DermaDefault" gen_menu_label(ply, text, posw, posh, font) end

-- Selection Perso (3 Slots)
function myperso_slots_perso_one_button() local sizew = ScrW()*0.25 local sizeh = ScrH()*0.65 local posw = ScrW()*0.2 local posh = ScrH()*0.35 local corner = 0 local color = Color(102, 102, 102, 200) local funca = function() return end gen_menu_button(ply, sizew, sizeh, posw, posh, corner, color, funca) end
function myperso_slots_perso_two_button() local sizew = ScrW()*0.25 local sizeh = ScrH()*0.65 local posw = ScrW()*0.45 local posh = ScrH()*0.35 local corner = 0 local color = Color(102, 102, 102, 200) local funca = function() return end gen_menu_button(ply, sizew, sizeh, posw, posh, corner, color, funca) end 
function myperso_slots_perso_three_button() local sizew = ScrW()*0.25 local sizeh = ScrH()*0.65 local posw = ScrW()*0.7 local posh = ScrH()*0.35 local corner = 0 local color = Color(102, 102, 102, 200) local funca = function() return end gen_menu_button(ply, sizew, sizeh, posw, posh, corner, color, funca) end

function myperso_slots_perso_one_label() local text = ""..MyPerso.Langue.new.."" local posw = ScrW()*0.3 local posh = ScrH()*0.5 local font = "DermaDefault" gen_menu_label(ply, text, posw, posh, font) end
function myperso_slots_perso_two_label() local text = ""..MyPerso.Langue.new.."" local posw = ScrW()*0.55 local posh = ScrH()*0.5 local font = "DermaDefault" gen_menu_label(ply, text, posw, posh, font) end
function myperso_slots_perso_three_label() local text = ""..MyPerso.Langue.new.."" local posw = ScrW()*0.75 local posh = ScrH()*0.5 local font = "DermaDefault" gen_menu_label(ply, text, posw, posh, font) end

-- Regroupement Function

function myperso_slots_perso()

    myperso_slots_perso_one_button()
    myperso_slots_perso_two_button()
    myperso_slots_perso_three_button()

    if slotsone == 1 then
        myperso_slots_perso_one_label()
    end
    if slotstwo == 1 then
        myperso_slots_perso_two_label()
    end
    if slotsthree == 1 then
        myperso_slots_perso_three_label()
    end

end

function myperso_infos_server()

    myperso_infos_server_frame()
    myperso_infos_server_label_name()
    myperso_infos_server_label_slots()

end

function myperso_main_panel()

    myperso_background()
    myperso_infos_server()
    myperso_slots_perso()

end